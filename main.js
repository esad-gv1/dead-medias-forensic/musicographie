const { app, BrowserWindow} = require('electron');

function createWindow() {
    
    const win = new BrowserWindow({
        width: 640,
        height: 480+35,
        webPreferences: {
            nodeIntegration: true
        }
    });

    win.loadFile('index.html');
    win.removeMenu();
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});
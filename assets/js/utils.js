
/**
* Get an element's position within the document taking into account the scroll positions
*
* @param {Element} el The DOM element
* @return {Object} the position of the element as {x, y}
*/
function getElementPosition(el) {
    let x = 0;
    let y = 0;

    while (el) {
        if (el.tagName === "BODY") {
            // deal with browser quirks with body/window/document and page scroll
            const xScroll = el.scrollLeft || document.documentElement.scrollLeft;
            const yScroll = el.scrollTop || document.documentElement.scrollTop;

            x += (el.offsetLeft - xScroll + el.clientLeft);
            y += (el.offsetTop - yScroll + el.clientTop);
        }
        else {
            // for all other non-BODY elements
            x += (el.offsetLeft - el.scrollLeft + el.clientLeft);
            y += (el.offsetTop - el.scrollTop + el.clientTop);
        }
        el = el.offsetParent;
    }
    return { x, y };
}
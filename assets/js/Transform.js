/**
* Transform that will be applied to a DOM objet through CSS 3D. Contains a position, rotation and scale object.
* Various methods permits to set and get the values for these objects
* @class Transform
* @param {Object} el
* @return {Object} description
*/
class Transform{
    
    constructor(el){
        
        this.el = el;
        //this.el.style.position = "absolute";

        /**
        * position vector 3
        * @property position
        */
        this.position = {};
        this.position.x = 0;
        this.position.y = 0;
        this.position.z = 0;
        
        /**
        * rotation vector 3
        * @property rotation
        */
        this.rotation = {};
        this.rotation.x = 0;
        this.rotation.y = 0;
        this.rotation.z = 0;
        
        /**
        * scale vector 2
        * @property scale
        */
        this.scale = {};
        this.scale.x = 1;
        this.scale.y = 1;
        
        if(this.el){
            this.el.style.transformOrigin = "top left 0";
        }
    }
    
    /**
    * Compute the CSS String that will be used to update the DOM element transform
    * @method makeCCS3DTransformString
    * @return {String} Css string
    */
    makeCCS3DTransformString(){
        var CSSString = "translate3d("+this.position.x+"px,"+this.position.y+"px,"+this.position.z+"px) ";
        CSSString += "rotateX("+this.rotation.x+"deg) rotateY("+this.rotation.y+"deg) rotateZ("+this.rotation.z+"deg) ";
        CSSString += "scale3d("+this.scale.x+","+this.scale.y+",1)";
        //console.log("CSSString : "+CSSString);
        return CSSString;
    };
    
    /**
    * Makes this element a node, or group, to ba able to manage children in space
    * @method setToNode
    */
    setToNode(){
        if(this.el){
            this.isNode = true;
            this.el.style.transformOrigin = "center center center";
        }
    };
    
    /**
    * set Transform Center of this element in local coordinates
    * @method setTransformCenter
    * @param {Object} x
    * @param {Object} y
    * @param {Object} z
    */
    setTransformCenter(x,y,z){
        if(this.el){
            this.el.style.transformOrigin= x +"px "+ y +"px "+ z +"px";
        }
    };
    
    /**
    * set Transform Center of this element in local coordinates with css values
    * @method setTransformCenterCSS
    * @param {String} x
    * @param {String} y
    * @param {String} z
    */
    setTransformCenterCSS(x,y,z){
        if(this.el){
            this.el.style.transformOrigin = x +" "+ y +" "+ z +"";
        }
    };
    
    /**
    * apply the transform to the DOM element !!Must be called for the transform to have any effect an the element!! Shortcut of applyTo() method
    * @method apply
    */
    apply(){
        if(this.el){
            this.applyTo(this.el);
        }
    };
    
    
    /**
    * apply the transform To the element.
    * @method applyTo
    * @param {Object} element
    */
    applyTo(element){
        
        element.style.position = "absolute";
        
        var style = this.makeCCS3DTransformString();
        
        element.style.webkitTransform = style;
        element.style.MozTransform = style;
        element.style.msTransform = style;
        element.style.OTransform = style;
        element.style.transform = style;
        
        element.style.webkitTransformStyle = "preserve-3d";
        element.style.MozTransformStyle = "preserve-3d";
        element.style.msTransformStyle = "preserve-3d";
        element.style.transformStyle = "preserve-3d";
        
    };
    
};